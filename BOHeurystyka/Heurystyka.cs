﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm
{
    public class LineState
    {
        public double Occupancy { get; set; }
        public int Halted { get; set; }
        public int StepsWithoutGreen { get; set; }
        public int DirectionsCount { get; set; }
    }

    enum TrafiicLight
    {
        GREEN = 'g', RED = 'r', YELLOW = 'y'
    }

    class ProfitFromState : IComparable<ProfitFromState>
    {
        public double Profit { get; set; }
        public string State { get; set; }

        public int CompareTo(ProfitFromState other)
        {
            if (other == null)
            {
                throw new ArgumentNullException("other");
            }
            return Profit.CompareTo(other.Profit);
        }
    }

    public class Algorithm
    {
        public static Random rand = new Random();

        [DefaultValue(90)]
        public double OccupancyAsFull { get; set; }

        [DefaultValue(0.02)]
        public double RandomThreshold { get; set; }

        [DefaultValue(2)]
        public int Factor { get; set; }

        [DefaultValue(3)]
        public int StepsWithoutGreen { get; set; }

        [DefaultValue(0.0)]
        public double MultidirectionLanePriority { get; set; }


        public string calculate(string[] traficLightsCombination, LineState[] lanes, int versionOfAlgorithm)
        {
            checkParameters(traficLightsCombination, lanes);

            SortedSet<ProfitFromState> profitsSet = new SortedSet<ProfitFromState>(new CombinationComparer());
            for (int i = 0; i < traficLightsCombination.Length; ++i)
            {
                double profit = profitFromCombination(traficLightsCombination[i], lanes, versionOfAlgorithm);
                profitsSet.Add(new ProfitFromState { Profit = profit, State = traficLightsCombination[i] });
            }
            return profitsSet.Max.State;
        }

        private class CombinationComparer : IComparer<ProfitFromState>
        {
            public int Compare(ProfitFromState x, ProfitFromState y)
            {
                return x.Profit.CompareTo(y.Profit);
            }
        }

        private double profitFromCombination(string combination, LineState[] lanes, int versionOfAlgorithm)
        {
            switch (versionOfAlgorithm)
            {
                case 1: return profitFromCombinationAlgorytmVer1(combination, lanes);
                case 2: return profitFromCombinationAlgorytmVer2(combination, lanes);
                case 3: return profitFromCombinationAlgorytmVer3(combination, lanes);
                default: throw new ArgumentException("wrong number of algorithm");
            }
        }

        private void checkParameters(string[] lightsCombinations, LineState[] lanes)
        {
            int combinationsLength = checkLightsCombinations(lightsCombinations);
            int lanesCount = checkLanes(lanes);
            if (combinationsLength != lanesCount) { throw new ArgumentException("number of lanes != lightsConfiguration length"); }
        }

        internal double profitFromCombinationAlgorytmVer1(string lightsCombination, LineState[] lanes)
        {
            TrafiicLight[] combination = splitLightCombination(lightsCombination);
            double profit = 0;
            for (int i = 0; i < combination.Length; ++i)
            {
                if (combination[i] == TrafiicLight.GREEN)
                {
                    if (lanes[i].Halted > 0)
                    {
                        profit += 1 / (double)lanes[i].DirectionsCount;
                    }
                }
            }
            Console.WriteLine(profit);
            return profit;
        }

        internal double profitFromCombinationAlgorytmVer2(string lightsCombination, LineState[] lanes)
        {
            TrafiicLight[] combination = splitLightCombination(lightsCombination);
            double profit = 0;
            for (int i = 0; i < combination.Length; ++i)
            {
                if (combination[i] == TrafiicLight.GREEN)
                {
                    if (lanes[i].Halted > 0)
                    {
                        if (lanes[i].Occupancy >= OccupancyAsFull)
                        {
                            profit += 2 / (double)lanes[i].DirectionsCount;
                        }
                        else
                        {
                            profit += 1 / (double)lanes[i].DirectionsCount;
                        }
                    }
                }
            }
            return profit;
        }

        internal double profitFromCombinationAlgorytmVer3(string lightsCombination, LineState[] lanes)
        {
            TrafiicLight[] combination = splitLightCombination(lightsCombination);
            double profit = 0;
            for (int i = 0; i < combination.Length; ++i)
            {
                if (combination[i] == TrafiicLight.GREEN)
                {
                    if (rand.NextDouble() < RandomThreshold)
                    {
                        profit += rand.Next(2);
                    }
                    else
                    {
                        var defaultProfit = ((lanes[i].DirectionsCount - 1)*MultidirectionLanePriority + 1)/
                                            (double)lanes[i].DirectionsCount;
                        if (lanes[i].Halted > 0)
                        {
                            if (lanes[i].Occupancy < OccupancyAsFull && lanes[i].StepsWithoutGreen < StepsWithoutGreen)
                            {
                                profit += defaultProfit;
                            }
                            else if (lanes[i].Occupancy > OccupancyAsFull && lanes[i].StepsWithoutGreen >= StepsWithoutGreen)
                            {
                                profit += Factor * Factor * defaultProfit;
                            }
                            else
                            {
                                profit += Factor * defaultProfit;
                            }
                        }
                    }
                }
            }
            return profit;
        }



        internal TrafiicLight[] splitLightCombination(string lightsCombination)
        {
            char[] literals = lightsCombination.ToCharArray();
            return literals.Select<char, TrafiicLight>(p => (TrafiicLight)p).ToArray<TrafiicLight>();
        }

        private int checkLightsCombinations(string[] lightsCombinations)
        {
            if (lightsCombinations == null) { throw new ArgumentNullException("lightsCombinations"); }
            if (lightsCombinations.Length <= 0) { throw new ArgumentException("lightsCombinations is empty"); }
            int lenght = lightsCombinations[0].Length;
            foreach (string combination in lightsCombinations)
            {
                if (combination.Length != lenght)
                {
                    throw new ArgumentException("different light combination's length, combination " + combination + " is different from rest");
                }
            }
            return lenght;
        }

        private int checkLanes(LineState[] lanes)
        {
            if (lanes == null) { throw new ArgumentNullException("lanes"); }
            return lanes.Length;
        }
    }


}
