from sndhdr import test_voc
import TrafficManager
SUMO_HOME = "./sumo/bin/"
NETWORKS = "./network/"
import os, sys
import subprocess

# we need to import python modules from the $SUMO_HOME/tools directory
try:

    tools = os.path.join(SUMO_HOME, 'tools')
    sys.path.append(tools)
    from sumolib import checkBinary
except ImportError:
    sys.exit("please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")

import traci
# the port used for communicating with your sumo instance
PORT = 8813
sumoBinary = checkBinary('sumo')

class Runner():
    testValues = {
        'occupancyAsFull':[50,60,70,80,90],
        'randomThreshold':[0.01,0.02,0.05,0.1],
        'factor':[2,3,4],
        'stepsWithoutGreen':[3,4,5],
        'multidirectionLanePriority':[0.0,0.25,0.5,0.75,1.0]
    }
    def __init__(self,networkName):
        self.__networkName = networkName

    def __enter__(self):
        self.__logFile = open(self.__networkName+".log", 'w')
        return self

    def __exit__(self,type,value,traceback):
        self.__logFile.close()


    def log(self,parameter,value,result):
        self.__logFile.write("Simulation for parameter %s: %2f with result: %d\n" % (parameter,value,result))

    def simulate(self,manager):
        i=0
        while traci.simulation.getMinExpectedNumber() > 0:
            if i%10==0:
                manager.computeStep()
            traci.simulationStep()
            i+=1
        self.__result = i

    def run(self):
        for parameter in self.testValues:
            for value in self.testValues[parameter]:

                sumoProcess = subprocess.Popen([sumoBinary, "-c", NETWORKS+self.__networkName+".sumocfg", "--remote-port", str(PORT)])
                traci.init(PORT)
                trafficManager = TrafficManager.TrafficManager()
                eval("trafficManager.setParameters("+parameter+"="+str(value)+")")
                self.simulate(trafficManager)
                self.log(parameter,value,self.__result)
                traci.close()



if __name__ == '__main__':
    networks = ['grid-big','grid-small','spider','hello','random-small']
    for network in networks:
        with Runner(network) as runner:
            runner.run()

