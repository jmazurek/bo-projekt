import os, sys
import clr
import re

SUMO_HOME = './sumo'

tools = os.path.join(SUMO_HOME, 'tools')
sys.path.append(tools)

import traci
PORT = 8813
clr.AddReference("BOHeurystyka.dll")
import Algorithm
import Algorithm.Algorithm as Heuristic
from System import Array

class TrafficManager:

    def __extractPhases(self,tlid):
        phasePattern = re.compile("phaseDef: ([RYGryg]+)")
        logicObjectString = traci.trafficlights.getCompleteRedYellowGreenDefinition(tlid)[0].__repr__()
        return map(lambda x: x.lower(),re.findall(phasePattern,logicObjectString))

    def __init__(self):
        self.__trafficLights = traci.trafficlights.getIDList()
        self.__phasesDict={}
        self.__convertedPhasesDict = {}
        for tl in self.__trafficLights:
           self.__phasesDict[tl] = self.__extractPhases(tl)
        self.__heuristic = Heuristic()

    def setParameters(self,occupancyAsFull=90,randomThreshold=0.02,factor=2,stepsWithoutGreen=3,multidirectionLanePriority=0.0):
        self.__heuristic.OccupancyAsFull = occupancyAsFull
        self.__heuristic.RandomThreshold = randomThreshold
        self.__heuristic.Factor = factor
        self.__heuristic.StepsWithoutGreen = stepsWithoutGreen
        self.__heuristic.MultidirectionLanePriority = multidirectionLanePriority




    def getTrafficLightInfo(self,tlid):
        controlledLanes = traci.trafficlights.getControlledLanes(tlid)
        trafficLightInfo = []
        for lane in controlledLanes:
            infoObject = Algorithm.LineState()
            infoObject.Occupancy = traci.lane.getLastStepOccupancy(lane)
            infoObject.Halted = traci.lane.getLastStepHaltingNumber(lane)
            infoObject.DirectionsCount = traci.lane.getLinkNumber(lane)
            infoObject.StepsWithoutGreen = traci.lane.getWaitingTime(lane)/10
            trafficLightInfo.append(infoObject)
        return trafficLightInfo

    def getPhases(self,tlid):
        return self.__phasesDict[tlid]


    def computeStep(self):
        for tl in self.__trafficLights:
            result = self.__heuristic.calculate(Array[str](self.getPhases(tl)),Array[Algorithm.LineState](self.getTrafficLightInfo(tl)),3)
            traci.trafficlights.setRedYellowGreenState(tl,result)





if __name__ == '__main__':
    manager = TrafficManager()
    i=0
    while traci.simulation.getMinExpectedNumber() > 0:
        #if i%10==0:
        #    manager.computeStep()
        traci.simulationStep()
        i+=1
    print i
    traci.close()


